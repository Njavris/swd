#ifndef __SWD_CMD_H__
#define __SWD_CMD_H__

#define START		(1 << 0)
#define DPACC		(0 << 1)
#define APACC		(1 << 1)
#define SWD_WR		(0 << 2)
#define SWD_RD		(1 << 2)
#define ADDR23(addr)	(((addr) & 0x3) << 3)
#define PARITY(b)	(((b) & 1) << 5)
#define STOP		(0 << 6)
#define PARK		(1 << 7)

#define CALC_PARHDR(p, r, a)	PARITY( \
				(p) ^ (r) ^ (a & 0x1) ^ \
				((a >> 1) & 0x1))
#define SWD_HEADER(p, r, a)	(START | (p << 1) | (r << 2) \
				| ADDR23(a) |  CALC_PARHDR(p, r, a) \
				| STOP | PARK)

#define SWD_WRITE	0
#define SWD_READ	1
#define SWD_DP		0
#define SWD_AP		1

#define SWD_DP_ABORT	SWD_HEADER(SWD_DP, SWD_WRITE, 0)
#define SWD_DP_CTRL_WR	SWD_HEADER(SWD_DP, SWD_WRITE, 1)
#define SWD_DP_SELECT	SWD_HEADER(SWD_DP, SWD_WRITE, 2)
//SWD_HEADER(SWD_DP, SWD_WRITE, 3)
#define SWD_DP_READ_ID	SWD_HEADER(SWD_DP, SWD_READ, 0)
#define SWD_DP_CTRL_RD	SWD_HEADER(SWD_DP, SWD_READ, 1)
#define SWD_DP_RESEND	SWD_HEADER(SWD_DP, SWD_READ, 2)
#define SWD_DP_RDBUFF	SWD_HEADER(SWD_DP, SWD_READ, 3)
#define SWD_AP_CTRL_WR	SWD_HEADER(SWD_AP, SWD_WRITE, 0)
#define SWD_AP_ADDR_WR	SWD_HEADER(SWD_AP, SWD_WRITE, 1)
//SWD_HEADER(SWD_AP, SWD_WRITE, 2)
#define SWD_AP_DATA_WR	SWD_HEADER(SWD_AP, SWD_WRITE, 3)
#define SWD_AP_CTRL_RD	SWD_HEADER(SWD_AP, SWD_READ, 0)
#define SWD_AP_ADDR_RD	SWD_HEADER(SWD_AP, SWD_READ, 1)
//SWD_HEADER(SWD_AP, SWD_READ, 2)
#define SWD_AP_DATA_RD	SWD_HEADER(SWD_AP, SWD_READ, 3)

#endif
