#ifndef __SWD_H__
#define __SWD_H__

#include "types.h"
#include "swd_ll.h"

struct swd_dev {
	struct swd_ll_dev ll_dev;
	int rst_pol;
	uint8_t ap;
	uint8_t bank;
	uint32_t dp_id;
	uint32_t ap_id;
	uint32_t dbg_rom;
	void (*write_select)(struct swd_dev *, unsigned, unsigned);
	int (*ap_read)(struct swd_dev *, unsigned, unsigned*);
	int (*ap_write)(struct swd_dev *, unsigned, unsigned);
	int (*dap_read)(struct swd_dev *, unsigned, unsigned*);
	int (*dap_write)(struct swd_dev *, unsigned, unsigned);
};

typedef enum arm_reg {
	ARM_R1 = 0,
	ARM_R2,
	ARM_R3,
	ARM_R4,
	ARM_R5,
	ARM_R6,
	ARM_R7,
	ARM_R8,
	ARM_R9,
	ARM_R10,
	ARM_R11,
	ARM_R12,
	ARM_SP,
	ARM_LR,
	ARM_PC,
	ARM_PSR,
	ARM_MSP,
	ARM_PSP,
} arm_reg_t;

struct arm_dev {
	struct swd_dev swd;
	uint32_t core_id;
	uint32_t chip_id;
	
	void (*set_vtor)(struct arm_dev*, uint32_t);
	void (*core_halt)(struct arm_dev*, uint32_t);
	void (*reg_write)(struct arm_dev*, arm_reg_t, uint32_t);
	void (*reg_read)(struct arm_dev*, arm_reg_t, uint32_t*);
	void (*writel)(struct arm_dev*, arm_reg_t, uint32_t);
	void (*readl)(struct arm_dev*, arm_reg_t, uint32_t*);
};

int swd_init(struct swd_dev *dev, unsigned clk_speed,
		int mosi, int clk, int rst_pin, int rst_pol);
int arm_init(struct arm_dev *dev);

#endif
