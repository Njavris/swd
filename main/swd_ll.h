#ifndef __SWD_LL_H__
#define __SWD_LL_H__

#include <driver/spi_master.h>
#include "types.h"
#include "swd_cmd.h"

struct swd_ll_dev {
	spi_device_handle_t spi_hndl;
	int rst_pin;
	int rst_pol;
	int (*ll_readl)(struct swd_ll_dev *, uint8_t, uint32_t*);
	int (*ll_writel)(struct swd_ll_dev *, uint8_t, uint32_t);
};

int swd_ll_init(struct swd_ll_dev *dev, unsigned clk_speed,
		int mosi, int clk, int rst_pin, int rst_pol);
#endif
