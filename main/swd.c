#include "swd.h"
#include "swd_cmd.h"

enum access {
	dp = 0,
	ap,
};

static uint32_t swd_readl(struct swd_dev *dev, enum access acc,
		uint32_t addr, uint32_t *val) {
	struct swd_ll_dev *ll_dev = &dev->ll_dev;
	return ll_dev->ll_readl(ll_dev, SWD_HEADER(acc, 1, addr), val);
}

static uint32_t swd_writel(struct swd_dev *dev, enum access acc,
		uint32_t addr, uint32_t val) {
	struct swd_ll_dev *ll_dev = &dev->ll_dev;
	return ll_dev->ll_writel(ll_dev, SWD_HEADER(acc, 0, addr), val);
}

static void write_select(struct swd_dev *dev, uint32_t ap, uint32_t bank) {
	uint32_t val;
	struct swd_ll_dev *ll_dev = &dev->ll_dev;
	if ((dev->ap == ap) && dev->bank == bank)
		return;
	dev->ap = ap;
	dev->bank = bank;
	val  = (bank & 0xf) << 4;
	val |= (ap & 0xff) << 24;
	ll_dev->ll_writel(ll_dev, SWD_DP_SELECT, val);
}

static uint32_t swd_readbuff(struct swd_dev *dev, uint32_t *val) {
	struct swd_ll_dev *ll_dev = &dev->ll_dev;
	return ll_dev->ll_readl(ll_dev, SWD_DP_RDBUFF, val);
}

static int dap_read(struct swd_dev *dev, uint32_t addr, uint32_t *val) {
	uint32_t rq_addr = (addr & 0xc) >> 2;
	swd_writel(dev, dp, 2, addr & 0xf);
	return swd_readl(dev, ap, rq_addr, val);
}

static int dap_write(struct swd_dev *dev, uint32_t addr, uint32_t val) {
	uint32_t rq_addr = (addr & 0xc) >> 2;
	swd_writel(dev, dp, 2, addr & 0xf);
	return swd_writel(dev, ap, rq_addr, val);
}

static int ap_read(struct swd_dev *dev, uint32_t addr, uint32_t *val) {
	struct swd_ll_dev *ll_dev = &dev->ll_dev;
	ll_dev->ll_writel(ll_dev, SWD_AP_ADDR_WR, addr);
	ll_dev->ll_readl(ll_dev, SWD_AP_DATA_RD, val);
	return swd_readbuff(dev, val);
}

static int ap_write(struct swd_dev *dev, uint32_t addr, uint32_t val) {
	struct swd_ll_dev *ll_dev = &dev->ll_dev;
	ll_dev->ll_writel(ll_dev, SWD_AP_ADDR_WR, addr);
	ll_dev->ll_writel(ll_dev, SWD_AP_DATA_WR, val);
	return swd_readbuff(dev, &val);
}

static void swd_connect(struct swd_dev *dev) {
	uint32_t val32;
	struct swd_ll_dev *ll_dev = &dev->ll_dev;

	dev->ap = 0xaa;
	dev->bank = 0xa;
	/* READ ID */
	ret = ll_dev->ll_readl(ll_dev, SWD_DP_READ_ID, &val32);
	debug("DP_ID: %08x\n", val32);
	dev->dp_id = val32;

	/* clear errors */
	val32 = 0xf << 1;
	ll_dev->ll_writel(ll_dev, SWD_DP_ABORT, val32);
	ll_dev->ll_readl(ll_dev, SWD_AP_CTRL_RD, &val32);

	write_select(dev, 0, 0);

	val32 = 0xf << 1;
	ll_dev->ll_writel(ll_dev, SWD_DP_ABORT, val32);
	ll_dev->ll_readl(ll_dev, SWD_DP_CTRL_RD, &val32);
	val32 = (1 << 28) | (1 << 30) | (1 << 26);
	ll_dev->ll_writel(ll_dev, SWD_DP_CTRL_WR, val32);
	ll_dev->ll_readl(ll_dev, SWD_DP_CTRL_RD, &val32);
	debug("SWD_DP_CTRL_RD: %08x\n", val32);
	
	/* wait untill power domains are activated */
	while (!(val32 & (1 << 29)))
		ll_dev->ll_readl(ll_dev, SWD_DP_CTRL_RD, &val32);

	while (!(val32 & (1 << 31)))
		ll_dev->ll_readl(ll_dev, SWD_DP_CTRL_RD, &val32);

	ll_dev->ll_readl(ll_dev, SWD_DP_CTRL_RD, &val32);
	debug("SWD_DP_CTRL_RD: %08x\n", val32);

	ll_dev->ll_readl(ll_dev, SWD_AP_CTRL_RD, &val32);
	val32 |= (0x23 << 24) | (1 << 6) | (1 << 4) | 2;
	ll_dev->ll_writel(ll_dev, SWD_AP_CTRL_WR, val32);
	ll_dev->ll_readl(ll_dev, SWD_AP_CTRL_RD, &val32);
	debug("CSW: %08x\n", val32);

	write_select(dev, 0x0, 0xf);

	ret = ll_dev->ll_readl(ll_dev, SWD_AP_DATA_RD, &val32);
	swd_readbuff(dev, &val32);
	debug("IDR: %08x\n", val32);
	dev->ap_id = val32;

	ret = ll_dev->ll_readl(ll_dev, SWD_HEADER(1, 1, 2), &val32);
	swd_readbuff(dev, &val32);
	debug("DEBUG_ROM_BASE_REG: %08x, masked: %08x\n",
			val32, val32 & 0xFFFFF000);
	dev->dbg_rom = val32;
	write_select(dev, 0x0, 0x0);
	dev->write_select = write_select;
	dev->ap_read = ap_read;
	dev->ap_write = ap_write;
	dev->dap_read = dap_read;
	dev->dap_write = dap_write;
}

int swd_init(struct swd_dev *dev, unsigned clk_speed,
		int mosi, int clk, int rst_pin, int rst_pol) {

	swd_ll_init(&dev->ll_dev, clk_speed, mosi, clk, rst_pin, rst_pol);

	swd_connect(dev);

	return 0;
}

#define DHCSR		0xe000edf0
#define DHCSR_KEY	0xa05f0000
#define CHIP_ID		0xe0042000
#define DCRSR		0xe000edf4
#define DCRDR		0xe000edf8
#define DEMCR		0xe000edfc
#define AIRCR		0xe000ed0c
#define AIRCR_KEY	0x05fa0000
#define VTOR		0xe000ed08


static void arm_core_halt(struct arm_dev *dev, uint32_t halt) {
	uint32_t reg_val = DHCSR_KEY;
	struct swd_dev *swd = &dev->swd;
	if (halt)
		reg_val |= 0x3;
	swd->ap_write(swd, DHCSR, reg_val);
}

static void arm_reg_read(struct arm_dev *dev, arm_reg_t reg, uint32_t *val) {
	struct swd_dev *swd = &dev->swd;
	uint32_t reg_val = reg & 0x1f;
	swd->ap_write(swd, DCRSR, reg_val);
	swd->ap_read(swd, DCRSR, val);
}

static void arm_reg_write(struct arm_dev *dev, arm_reg_t reg, uint32_t val) {
	struct swd_dev *swd = &dev->swd;
	uint32_t reg_val = reg & 0x1f;
	reg_val |= 1 << 16;
	swd->ap_write(swd, DCRSR, val);
	swd->ap_write(swd, DCRSR, reg_val);
}

static void arm_mem_readl(struct arm_dev *dev, uint32_t addr, uint32_t *val) {
	struct swd_dev *swd = &dev->swd;
	swd->ap_read(swd, addr, val);
}

static void arm_mem_writel(struct arm_dev *dev, uint32_t addr, uint32_t val) {
	struct swd_dev *swd = &dev->swd;
	swd->ap_write(swd, addr, val);
}

static void arm_set_vtor(struct arm_dev *dev, uint32_t addr) {
	arm_mem_writel(dev, VTOR, addr);
}

int arm_init(struct arm_dev *dev) {
	struct swd_dev *swd = &dev->swd;
	swd->write_select(swd, 0x0, 0x0);
	swd->ap_read(swd, CHIP_ID, &dev->chip_id);
	debug("CORE_ID: %08x\n", dev->chip_id);
	dev->set_vtor = arm_set_vtor;
	dev->core_halt = arm_core_halt;
	dev->reg_write = arm_reg_write;
	dev->reg_read = arm_reg_read;
	dev->writel = arm_mem_writel;
	dev->readl = arm_mem_readl;

	return 0;
}
