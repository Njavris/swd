/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "string.h"

static struct swd_dev {
	spi_device_handle_t spi_hndl;
} swd;

#define START		(1 << 0)
#define DPACC		(0 << 1)
#define APACC		(1 << 1)
#define SWD_WR		(0 << 2)
#define SWD_RD		(1 << 2)
#define ADDR23(addr)	(((addr) & 0x3) << 3)
#define PARITY(b)	(((b) & 1) << 5)
#define STOP		(0 << 6)
#define PARK		(1 << 7)

#define ACK_OK		(1 << 0)
#define ACK_WAIT	(1 << 1)
#define ACK_FAIL	(1 << 2)
#define ACK_PROT_ERR	(0)


#define CALC_PARHDR(p, r, a)	PARITY((p) ^ (r) ^ (a & 0x1) ^ ((a >> 1) & 0x1))
#define SWD_HEADER(p, r, a)	(START | (p << 1) | (r << 2) | ADDR23(a) |  CALC_PARHDR(p, r, a) | STOP | PARK)

/* DP */
/* WRITE */
#define SWD_ABORT	SWD_HEADER(0, 0, 0)
#define SWD_DP_CTRL_WR	SWD_HEADER(0, 0, 1)
#define SWD_SELECT	SWD_HEADER(0, 0, 2)
//SWD_HEADER(0, 0, 3)
/* READ */
#define SWD_READ_ID	SWD_HEADER(0, 1, 0)
#define SWD_DP_CTRL_RD	SWD_HEADER(0, 1, 1)
#define SWD_RESEND	SWD_HEADER(0, 1, 2)
#define SWD_RDBUFF	SWD_HEADER(0, 1, 3)

/* AP */
/* WRITE */
#define SWD_AP_CTRL_WR	SWD_HEADER(1, 0, 0)
#define SWD_AP_ADDR_WR	SWD_HEADER(1, 0, 1)
//SWD_HEADER(1, 0, 2)
#define SWD_AP_DATA_WR	SWD_HEADER(1, 0, 3)
/* READ */
#define SWD_AP_CTRL_RD	SWD_HEADER(1, 1, 0)
#define SWD_AP_ADDR_RD	SWD_HEADER(1, 1, 1)
//SWD_HEADER(1, 1, 2)
#define SWD_AP_DATA_RD	SWD_HEADER(1, 1, 3)

int swd_tx(struct swd_dev *swd, uint8_t *data, int bits) {
	int data_len, ret = 0;
	spi_transaction_t trans;
	memset(&trans, 0, sizeof(spi_transaction_t));
	trans.flags = SPI_TRANS_USE_TXDATA;
	trans.length = bits;

	data_len = bits / 8;
	if (bits % 8)
		data_len++;

	for (int i = 0; i < data_len; i++) {
		trans.tx_data[i] = data[i];
	}

//	gpio_set_level(GPIO_NUM_4, 1);

	if (spi_device_transmit(swd->spi_hndl, &trans)) {
		ret = -1;
		goto out;
	}
//	gpio_set_level(GPIO_NUM_4, 0);

out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}

int swd_rx(struct swd_dev *swd, uint8_t *data, int bits) {
	int cpy_len, ret = 0;
	spi_transaction_t trans;
	memset(&trans, 0, sizeof(spi_transaction_t));

	trans.flags = SPI_TRANS_USE_RXDATA;
	trans.rxlength = bits;
//	gpio_set_level(33, 0);

	if (spi_device_transmit(swd->spi_hndl, &trans)) {
		ret = -1;
		goto out;
	}

	cpy_len = bits / 8;
	if (bits % 8)
		cpy_len++;
	memcpy(data, &trans.rx_data, cpy_len);

out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}

static int swd_init(void) {
	int ret = 0;
	spi_bus_config_t spi_bus_cfg = {
//		.miso_io_num = 32,
		.mosi_io_num = GPIO_NUM_33,
		.sclk_io_num = GPIO_NUM_25,
		.quadwp_io_num = -1,
		.quadhd_io_num = -1,
		.max_transfer_sz = 0,
	};

	spi_device_interface_config_t spi_if_cfg = {
//		.clock_speed_hz = 10000000,
		.clock_speed_hz = 10000,
		.command_bits = 0,
		.address_bits = 0,
		.dummy_bits = 0,
		.mode = 0,
		.duty_cycle_pos = 0,
		.cs_ena_pretrans = 0,
		.cs_ena_posttrans = 0,
		.spics_io_num = -1,
		.flags = SPI_DEVICE_3WIRE | SPI_DEVICE_HALFDUPLEX | SPI_DEVICE_BIT_LSBFIRST,
		.queue_size = 24,
	};

	if (spi_bus_initialize(HSPI_HOST, &spi_bus_cfg, 0)) {
		ret = -1;
		goto out;
	}
	if (spi_bus_add_device(HSPI_HOST, &spi_if_cfg, &swd.spi_hndl)) {
		ret = -2;
		goto out;
	}
out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}


unsigned get_swd_ack(void) {
	unsigned ret = 0;
	swd_rx(&swd, (uint8_t*)&ret, 3);
	return ret;
}

void swd_trn(int clks) {
	uint8_t dummy = 0;
	swd_rx(&swd, &dummy, clks);
}

int swd_writel(struct swd_dev *swd, uint8_t cmd, unsigned val);
int swd_readl(struct swd_dev *swd, uint8_t cmd, unsigned *val);

int swd_readl(struct swd_dev *swd, uint8_t cmd, unsigned *val) {
	unsigned ack;
	uint8_t par;

	swd_tx(swd, &cmd, 8);
	swd_trn(1);
	ack = get_swd_ack();
	if (ack != ACK_OK) {
		unsigned tmp;
//		printf("%s: CMD: %02x ACK: %x\n", __func__, cmd, ack);
		swd_trn(1);
		*val = 0;
		if (ack == ACK_WAIT) {
//printf("ACK_WAIT\n");
// TODO: limit recursion
			ack = swd_readl(swd, cmd, val);
			swd_readl(swd, SWD_RDBUFF, val);
			return ack;
		}

		if (ack == ACK_FAIL) {
			swd_readl(swd, SWD_DP_CTRL_RD, &tmp);
			/* clear STICKYERR bit */
			if (tmp & (1 << 5))
				tmp &= ~(1 << 5);
			swd_writel(swd, SWD_DP_CTRL_WR, tmp);
		}

		return ack;
	}
	swd_rx(swd, (uint8_t*)val, 32);
	swd_rx(swd, &par, 1);
	swd_trn(1);

	return ack;
}

int swd_writel(struct swd_dev *swd, uint8_t cmd, unsigned val) {
	unsigned ack;
	uint8_t par = 0;

	for (int i = 0; i < 32; i++)
		par ^= (val >> i) & 1;

	swd_tx(swd, &cmd, 8);
	swd_trn(1);
	ack = get_swd_ack();
	swd_trn(1);
	if (ack != ACK_OK) {
//		printf("%s: CMD: %02x ACK: %x\n", __func__, cmd, ack);
		if (ack == ACK_WAIT) {
//printf("ACK_WAIT\n");
// TODO: limit recursion
			ack = swd_writel(swd, cmd, val);
			return ack;
		}
//		if (ack == ACK_FAIL) {
//			unsigned tmp;
//			swd_readl(swd, SWD_DP_CTRL_RD, &tmp);
//			/* clear STICKYERR bit */
//			if (tmp & (1 << 5))
//				tmp &= ~(1 << 5);
//			swd_writel(swd, SWD_DP_CTRL_WR, tmp);
//		}
		return ack;
	}

	swd_tx(swd, (uint8_t*)&val, 32);
	swd_tx(swd, &par, 1);
	return ack;
}
unsigned dap_read(unsigned addr) {
	unsigned ret, rq_addr |= (address & 0xc) >> 2;
	swd_writel(&swd, SWD_SELECT, addr & 0xf);
	swd_readl(&swd, SWD_HEADER(1, 1, rq_addr), &ret);
	return ret;
}

void dap_write(unsigned addr, unsigned val) {
	unsigned rq_addr |= (address & 0xc) >> 2;
	swd_writel(&swd, SWD_SELECT, addr & 0xf);
	swd_writel(&swd, SWD_HEADER(1, 1, rq_addr), val);
}

void config_swd(void) {
	unsigned id;
	unsigned val32 = ~0;
	uint16_t val16 = 0xe79e;
	int ret;

	/* switch mode from JTAG to SWD */
	/* 51 or more clks with data line high change to RESET state */
	swd_tx(&swd, (uint8_t*)&val32, 32);
	swd_tx(&swd, (uint8_t*)&val32, 32);
	/* 16 bit sequence: 0b0111100111100111 switch from JTAG to SWD*/
	swd_tx(&swd, (uint8_t*)&val16, 16);
	/* 51 or more clks with data line high change to RESET state */
	swd_tx(&swd, (uint8_t*)&val32, 32);
	swd_tx(&swd, (uint8_t*)&val32, 32);
	/* 2 or more clks with data line low change to IDLE state */
	val32 = 0;
	swd_tx(&swd, (uint8_t*)&val32, 8);

	/* READ ID */
	ret = swd_readl(&swd, SWD_READ_ID, &id);
	if (ret) {
		printf("%s: ack: %d\n", __func__, ret);
	}
	printf("SWD_ID: %08x\n", id);
	val32 = 0xf;
	swd_writel(&swd, SWD_ABORT, val32);

	swd_readl(&swd, SWD_DP_CTRL_RD, &val32);
	val32 = (1 << 28) | (1 << 30) | (1 << 5);
	swd_writel(&swd, SWD_DP_CTRL_WR, val32);
	swd_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("SWD_DP_CTRL_RD: %08x\n", val32);

	/* wait untill power domains are activated */
	while (!(val32 & (1 << 29)))
		swd_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("CDBGPWRUPACK\n");

	while (!(val32 & (1 << 29)))
		swd_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("CSYSPWRUPACK\n");

	/* activat overrun checking */
	val32 = (1 << 30) | (1 << 28) | 1;
	swd_writel(&swd, SWD_DP_CTRL_WR, val32);

	swd_readl(&swd, SWD_DP_CTRL_RD, &val32);
	printf("SWD_DP_CTRL_RD: %08x\n", val32);

	swd_writel(&swd, SWD_SELECT, 0x0);
	swd_writel(&swd, SWD_AP_ADDR_WR, 0xc);

	ret = swd_readl(&swd, SWD_AP_DATA_RD, &val32);
	if (ret) {
		printf("%s: ack: %d\n", __func__, ret);
	}
	printf("core_id :%08x\n", val32);
}

void target_rst(void) {
	gpio_set_level(CONFIG_STM32_NRST, 0);
	vTaskDelay(100 / portTICK_PERIOD_MS);
	gpio_set_level(CONFIG_STM32_NRST, 1);
}

void app_main(void)
{
/*
https://www.cnblogs.com/shangdawei/p/4748751.html
https://www.cnblogs.com/shangdawei/p/4757044.html
*/
	unsigned char c;
	int uart_num = UART_NUM_2;
	unsigned val;
	int ret;

	uart_config_t uart_config = {
		.baud_rate = 5000000,
		.data_bits = UART_DATA_8_BITS,
		.parity = UART_PARITY_DISABLE,
		.stop_bits = UART_STOP_BITS_1,
		.flow_ctrl = UART_HW_FLOWCTRL_DISABLE
	};
	uart_param_config(uart_num, &uart_config);

	gpio_pad_select_gpio(CONFIG_STM32_NRST);
	gpio_set_direction(CONFIG_STM32_NRST, GPIO_MODE_OUTPUT);
	gpio_set_level(CONFIG_STM32_NRST, 1);

	swd_init();
	config_swd();



	swd_writel(&swd, SWD_SELECT, 0x0);
//	swd_readl(&swd, SWD_DP_CTRL_RD, &val);
//	val = (1 << 28) | (1 << 30);
//	swd_writel(&swd, SWD_DP_CTRL_WR, val); 
//	val = (1 << 31) | (1 << 2);
//	swd_readl(&swd, SWD_DP_CTRL_RD, &val);
//	swd_writel(&swd, SWD_AP_CTRL_WR, val);

	ret = swd_readl(&swd, SWD_AP_CTRL_RD, &val);
	val |= 2 | (1 << 6) | (1 << 4);
	swd_writel(&swd, SWD_AP_CTRL_WR, val);



	unsigned address = 0x80000000;
//	unsigned step = 0x1000;
	swd_writel(&swd, SWD_SELECT, 0x2);

//	unsigned address = 0xE000ED00;//0x410CC200;//0xE0042000;
	while(1) {
			ret = swd_writel(&swd, SWD_AP_ADDR_WR, address);
//			swd_writel(&swd, SWD_AP_ADDR_WR, 0x40015800);
//			ret = swd_writel(&swd, SWD_HEADER(1, 1, 2), 0xE0042000);
//			printf("0xf8 :%08x\n", val);
//			ret = swd_writel(&swd, SWD_AP_DATA_WR, 0xA05F0003);

			ret = swd_readl(&swd, SWD_AP_DATA_RD, &val);
			printf("%08x :%08x\n", address, val);
			address += 4;
//			ret = swd_readl(&swd, SWD_AP_CTRL_RD, &val);
//			printf("SWD_AP_CTRL_RD :%08x\n", val);


//printf("Looking at range (%08x:%08x)\n", addr, addr + step);
//		for (int off = 0; off < step; off += 4) {
//			ret = swd_writel(&swd, SWD_AP_ADDR_WR, addr + off);
//			if (ret != ACK_OK) continue;
//			ret = swd_readl(&swd, SWD_AP_DATA_RD, &val);
//			if (ret != ACK_OK) continue;
//			if (val)
//				printf("%08x :%08x\n", addr + off, val);
//		}
//		addr += step;;

		vTaskDelay(1000 / portTICK_PERIOD_MS);
	};
}
