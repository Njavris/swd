#ifndef __SWD_LL__
#define __SWD_LL__

#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "esp_system.h"
#include "string.h"

#define START		(1 << 0)
#define DPACC		(0 << 1)
#define APACC		(1 << 1)
#define SWD_WR		(0 << 2)
#define SWD_RD		(1 << 2)
#define ADDR23(addr)	(((addr) & 0x3) << 3)
#define PARITY(b)	(((b) & 1) << 5)
#define STOP		(0 << 6)
#define PARK		(1 << 7)

#define ACK_OK		(1 << 0)
#define ACK_WAIT	(1 << 1)
#define ACK_FAIL	(1 << 2)
#define ACK_PROT_ERR	(0)


#define CALC_PARHDR(p, r, a)	PARITY((p) ^ (r) ^ (a & 0x1) ^ ((a >> 1) & 0x1))
#define SWD_HEADER(p, r, a)	(START | (p << 1) | (r << 2) | ADDR23(a) |  CALC_PARHDR(p, r, a) | STOP | PARK)


/* DP */
/* WRITE */
#define SWD_ABORT	SWD_HEADER(0, 0, 0)
#define SWD_DP_CTRL_WR	SWD_HEADER(0, 0, 1)
#define SWD_SELECT	SWD_HEADER(0, 0, 2)
//SWD_HEADER(0, 0, 3)
/* READ */
#define SWD_READ_ID	SWD_HEADER(0, 1, 0)
#define SWD_DP_CTRL_RD	SWD_HEADER(0, 1, 1)
#define SWD_RESEND	SWD_HEADER(0, 1, 2)
#define SWD_RDBUFF	SWD_HEADER(0, 1, 3)

/* AP */
/* WRITE */
#define SWD_AP_CTRL_WR	SWD_HEADER(1, 0, 0)
#define SWD_AP_ADDR_WR	SWD_HEADER(1, 0, 1)
//SWD_HEADER(1, 0, 2)
#define SWD_AP_DATA_WR	SWD_HEADER(1, 0, 3)
/* READ */
#define SWD_AP_CTRL_RD	SWD_HEADER(1, 1, 0)
#define SWD_AP_ADDR_RD	SWD_HEADER(1, 1, 1)
//SWD_HEADER(1, 1, 2)
#define SWD_AP_DATA_RD	SWD_HEADER(1, 1, 3)

struct swd_dev {
	spi_device_handle_t spi_hndl;
	int pin_nrst;
};

extern struct swd_dev swd;

int swd_ll_init(unsigned clk_speed, int mosi, int clk, int nrst);
int swd_ll_writel(struct swd_dev *swd, uint8_t cmd, unsigned val);
int swd_ll_readl(struct swd_dev *swd, uint8_t cmd, unsigned *val);
void swd_ll_rst(struct swd_dev *swd);
void swd_ll_init_seq(struct swd_dev *swd);
#endif
