#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "swd_ll.h"

struct swd_dev swd;

int swd_ll_init(unsigned clk_speed, int mosi, int clk, int nrst) {
	int ret = 0;

	swd.pin_nrst = nrst;
	gpio_pad_select_gpio(swd.pin_nrst);
	gpio_set_direction(swd.pin_nrst, GPIO_MODE_OUTPUT);
	gpio_set_level(swd.pin_nrst, 1);

	spi_bus_config_t spi_bus_cfg = {
		.mosi_io_num = mosi,
		.sclk_io_num = clk,
		.quadwp_io_num = -1,
		.quadhd_io_num = -1,
		.max_transfer_sz = 0,
	};

	spi_device_interface_config_t spi_if_cfg = {
		.clock_speed_hz = clk_speed,
		.command_bits = 0,
		.address_bits = 0,
		.dummy_bits = 0,
		.mode = 0,
		.duty_cycle_pos = 0,
		.cs_ena_pretrans = 0,
		.cs_ena_posttrans = 0,
		.spics_io_num = -1,
		.flags = SPI_DEVICE_3WIRE | SPI_DEVICE_HALFDUPLEX |
					SPI_DEVICE_BIT_LSBFIRST,
		.queue_size = 24,
	};

	if (spi_bus_initialize(HSPI_HOST, &spi_bus_cfg, 0)) {
		ret = -1;
		goto out;
	}
	if (spi_bus_add_device(HSPI_HOST, &spi_if_cfg, &swd.spi_hndl)) {
		ret = -2;
		goto out;
	}
out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;

}

static int swd_tx(struct swd_dev *swd, uint8_t *data, int bits) {
	int data_len, ret = 0;
	spi_transaction_t trans;
	memset(&trans, 0, sizeof(spi_transaction_t));
	trans.flags = SPI_TRANS_USE_TXDATA;
	trans.length = bits;

	data_len = bits / 8;
	if (bits % 8)
		data_len++;

	for (int i = 0; i < data_len; i++) {
		trans.tx_data[i] = data[i];
	}

//	gpio_set_level(GPIO_NUM_4, 1);

	if (spi_device_transmit(swd->spi_hndl, &trans)) {
		ret = -1;
		goto out;
	}
//	gpio_set_level(GPIO_NUM_4, 0);

out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}

static int swd_rx(struct swd_dev *swd, uint8_t *data, int bits) {
	int cpy_len, ret = 0;
	spi_transaction_t trans;
	memset(&trans, 0, sizeof(spi_transaction_t));

	trans.flags = SPI_TRANS_USE_RXDATA;
	trans.rxlength = bits;
//	gpio_set_level(33, 0);

	if (spi_device_transmit(swd->spi_hndl, &trans)) {
		ret = -1;
		goto out;
	}

	cpy_len = bits / 8;
	if (bits % 8)
		cpy_len++;
	memcpy(data, &trans.rx_data, cpy_len);

out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}

static unsigned get_swd_ack(void) {
	unsigned ret = 0;
	swd_rx(&swd, (uint8_t*)&ret, 3);
	return ret;
}

static void swd_trn(int clks) {
	uint8_t dummy = 0;
	swd_rx(&swd, &dummy, clks);
}

int swd_ll_readl(struct swd_dev *swd, uint8_t cmd, unsigned *val) {
	unsigned ack;
	uint8_t par;

	swd_tx(swd, &cmd, 8);
	swd_trn(1);
	ack = get_swd_ack();
	if (ack != ACK_OK) {
		//printf("%s: CMD: %02x ACK: %x\n", __func__, cmd, ack);
		swd_trn(1);
		*val = 0;
		if (ack == ACK_WAIT) {
			//printf("ACK_WAIT\n");
			ack = swd_ll_readl(swd, cmd, val);
			swd_ll_writel(swd, SWD_ABORT, 0xf << 1);
			return ack;
		}

		if (ack == ACK_FAIL) {
			unsigned tmp;
			swd_ll_readl(swd, SWD_DP_CTRL_RD, &tmp);
			printf("ACK_FAIL STS:%08x\n", tmp);
			swd_ll_writel(swd, SWD_ABORT, 0xf << 1);

			/* clear STICKYERR bit */
			if (tmp & (1 << 5))
				tmp &= ~(1 << 5);
			swd_ll_writel(swd, SWD_DP_CTRL_WR, tmp);
			return ack;
		}
	}
	swd_rx(swd, (uint8_t*)val, 32);
	swd_rx(swd, &par, 1);
	swd_trn(1);

	return ack;
}

int swd_ll_writel(struct swd_dev *swd, uint8_t cmd, unsigned val) {
	unsigned ack;
	uint8_t par = 0;

	for (int i = 0; i < 32; i++)
		par ^= (val >> i) & 1;

	swd_tx(swd, &cmd, 8);
	swd_trn(1);
	ack = get_swd_ack();
	swd_trn(1);
	if (ack != ACK_OK) {
		//printf("%s: CMD: %02x ACK: %x\n", __func__, cmd, ack);
		if (ack == ACK_WAIT) {
			//printf("ACK_WAIT\n");
			swd_ll_writel(swd, SWD_ABORT, 0xf << 1);
			ack = swd_ll_writel(swd, cmd, val);
			return ack;
		}
		if (ack == ACK_FAIL) {
			unsigned tmp;
			swd_ll_readl(swd, SWD_DP_CTRL_RD, &tmp);
			printf("ACK_FAIL STS:%08x\n", tmp);
			swd_ll_writel(swd, SWD_ABORT, 0xf << 1);

			/* clear STICKYERR bit */
			if (tmp & (1 << 5))
				tmp &= ~(1 << 5);
			swd_ll_writel(swd, SWD_DP_CTRL_WR, tmp);
		}
		return ack;
	}

	swd_tx(swd, (uint8_t*)&val, 32);
	swd_tx(swd, &par, 1);
	return ack;
}

void swd_ll_rst(struct swd_dev *swd) {
	gpio_set_level(swd->pin_nrst, 0);
	vTaskDelay(100 / portTICK_PERIOD_MS);
	gpio_set_level(swd->pin_nrst, 1);
}

void swd_ll_init_seq(struct swd_dev *swd) {
	unsigned val32 = ~0;
	uint16_t val16 = 0xe79e;
	int ret;

	/* switch mode from JTAG to SWD */
	/* 51 or more clks with data line high change to RESET state */
	swd_tx(swd, (uint8_t*)&val32, 32);
	swd_tx(swd, (uint8_t*)&val32, 32);
	/* 16 bit sequence: 0b0111100111100111 switch from JTAG to SWD*/
	swd_tx(swd, (uint8_t*)&val16, 16);
	/* 51 or more clks with data line high change to RESET state */
	swd_tx(swd, (uint8_t*)&val32, 32);
	swd_tx(swd, (uint8_t*)&val32, 32);
	/* 2 or more clks with data line low change to IDLE state */
	val32 = 0;
	swd_tx(swd, (uint8_t*)&val32, 8);
}
