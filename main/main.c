#include <freertos/FreeRTOS.h>
#include <string.h>
#include "types.h"
#include "swd.h"

struct arm_dev arm;

void app_main(void) {
	swd_init(&arm.swd, CONFIG_SPI_SPEED, CONFIG_SPI_MOSI_PIN,
		CONFIG_SPI_CLK_PIN, CONFIG_STM32_RST_PIN,
		CONFIG_STM32_RST_POL);
	arm_init(&arm);
}

