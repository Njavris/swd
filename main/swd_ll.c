#include "swd_ll.h"
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_system.h>
#include <string.h>

#define ACK_OK		(1 << 0)
#define ACK_WAIT	(1 << 1)
#define ACK_FAIL	(1 << 2)
#define ACK_PROT_ERR	(0)

static void swd_reset_assert(struct swd_ll_dev *dev, int assert) {
	if (dev->rst_pin != -1)
		gpio_set_level(dev->rst_pin, !(dev->rst_pol ^ !!assert));
}

static void swd_hw_reset(struct swd_ll_dev *dev, int delay_ms) {
	swd_reset_assert(dev, 1);
	vTaskDelay(delay_ms / portTICK_PERIOD_MS);
	swd_reset_assert(dev, 0);
}

static int swd_tx(struct swd_ll_dev *swd, uint8_t *data, int bits) {
	int data_len, ret = 0;
	spi_transaction_t trans;
	memset(&trans, 0, sizeof(spi_transaction_t));
	trans.flags = SPI_TRANS_USE_TXDATA;
	trans.length = bits;

	data_len = bits / 8;
	if (bits % 8)
		data_len++;

	for (int i = 0; i < data_len; i++) {
		trans.tx_data[i] = data[i];
	}

	if (spi_device_transmit(swd->spi_hndl, &trans)) {
		ret = -1;
		goto out;
	}
out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}

static int swd_rx(struct swd_ll_dev *swd, uint8_t *data, int bits) {
	int cpy_len, ret = 0;
	spi_transaction_t trans;
	memset(&trans, 0, sizeof(spi_transaction_t));

	trans.flags = SPI_TRANS_USE_RXDATA;
	trans.rxlength = bits;

	if (spi_device_transmit(swd->spi_hndl, &trans)) {
		ret = -1;
		goto out;
	}

	cpy_len = bits / 8;
	if (bits % 8)
		cpy_len++;
	memcpy(data, &trans.rx_data, cpy_len);

out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;
}

static unsigned swd_get_ack(struct swd_ll_dev *dev) {
	unsigned ret = 0;
	swd_rx(dev, (uint8_t*)&ret, 3);
	return ret;
}

static void swd_trn(struct swd_ll_dev *dev, int clks) {
	uint8_t dummy = 0;
	swd_rx(dev, &dummy, clks);
}

int swd_ll_readl(struct swd_ll_dev *dev, uint8_t cmd, unsigned *val) {
	unsigned ack;
	uint8_t par;

	swd_tx(dev, &cmd, 8);
	swd_trn(dev, 1);
	ack = swd_get_ack(dev);
	if (ack != ACK_OK) {
		//printf("%s: CMD: %02x ACK: %x\n", __func__, cmd, ack);
		swd_trn(dev, 1);
		*val = 0;
		if (ack == ACK_WAIT) {
			//printf("ACK_WAIT\n");
			dev->ll_writel(dev, SWD_DP_ABORT, 0xf << 1);
			ack = dev->ll_readl(dev, cmd, val);
			return ack;
		}

		if (ack == ACK_FAIL) {
			unsigned tmp;
			dev->ll_readl(dev, SWD_DP_CTRL_RD, &tmp);
			debug("ACK_FAIL STS:%08x\n", tmp);
			dev->ll_writel(dev, SWD_DP_ABORT, 0xf << 1);

			/* clear STICKYERR bit */
			if (tmp & (1 << 5))
				tmp &= ~(1 << 5);
			dev->ll_writel(dev, SWD_DP_CTRL_WR, tmp);
			return ack;
		}
	}
	swd_rx(dev, (uint8_t*)val, 32);
	swd_rx(dev, &par, 1);
	swd_trn(dev, 1);

	return ack;
}

int swd_ll_writel(struct swd_ll_dev *dev, uint8_t cmd, unsigned val) {
	unsigned ack;
	uint8_t par = 0;

	for (int i = 0; i < 32; i++)
		par ^= (val >> i) & 1;

	swd_tx(dev, &cmd, 8);
	swd_trn(dev, 1);
	ack = swd_get_ack(dev);
	swd_trn(dev, 1);
	if (ack != ACK_OK) {
		//printf("%s: CMD: %02x ACK: %x\n", __func__, cmd, ack);
		if (ack == ACK_WAIT) {
			//printf("ACK_WAIT\n");
			dev->ll_writel(dev, SWD_DP_ABORT, 0xf << 1);
			ack = dev->ll_writel(dev, cmd, val);
			return ack;
		}
		if (ack == ACK_FAIL) {
			unsigned tmp;
			dev->ll_readl(dev, SWD_DP_CTRL_RD, &tmp);
			debug("ACK_FAIL STS:%08x\n", tmp);
			dev->ll_writel(dev, SWD_DP_ABORT, 0xf << 1);

			/* clear STICKYERR bit */
			if (tmp & (1 << 5))
				tmp &= ~(1 << 5);
			dev->ll_writel(dev, SWD_DP_CTRL_WR, tmp);
		}
		return ack;
	}

	swd_tx(dev, (uint8_t*)&val, 32);
	swd_tx(dev, &par, 1);
	return ack;
}

static void swd_ll_init_seq(struct swd_ll_dev *dev) {
	uint32_t val32 = ~0;
	uint16_t val16 = 0xe79e;

	/* switch mode from JTAG to SWD */
	/* 51 or more clks with data line high change to RESET state */
	swd_tx(dev, (uint8_t*)&val32, 32);
	swd_tx(dev, (uint8_t*)&val32, 32);
	/* 16 bit sequence: 0b0111100111100111 switch from JTAG to SWD*/
	swd_tx(dev, (uint8_t*)&val16, 16);
	/* 51 or more clks with data line high change to RESET state */
	swd_tx(dev, (uint8_t*)&val32, 32);
	swd_tx(dev, (uint8_t*)&val32, 32);
	/* 2 or more clks with data line low change to IDLE state */
	val32 = 0;
	swd_tx(dev, (uint8_t*)&val32, 8);
}

int swd_ll_init(struct swd_ll_dev *dev, unsigned clk_speed,
		int mosi, int clk, int rst_pin, int rst_pol) {
	int ret = 0;

	dev->rst_pin = rst_pin;
	dev->rst_pol = rst_pol;
	if (dev->rst_pin != -1) {
		gpio_pad_select_gpio(dev->rst_pin);
		gpio_set_direction(dev->rst_pin, GPIO_MODE_OUTPUT);
		swd_hw_reset(dev, 1);
	}

	spi_bus_config_t spi_bus_cfg = {
		.mosi_io_num = mosi,
		.sclk_io_num = clk,
		.quadwp_io_num = -1,
		.quadhd_io_num = -1,
		.max_transfer_sz = 0,
	};

	spi_device_interface_config_t spi_if_cfg = {
		.clock_speed_hz = clk_speed,
		.command_bits = 0,
		.address_bits = 0,
		.dummy_bits = 0,
		.mode = 0,
		.duty_cycle_pos = 0,
		.cs_ena_pretrans = 0,
		.cs_ena_posttrans = 0,
		.spics_io_num = -1,
		.flags = SPI_DEVICE_3WIRE | SPI_DEVICE_HALFDUPLEX |
					SPI_DEVICE_BIT_LSBFIRST,
		.queue_size = 24,
	};

	if (spi_bus_initialize(HSPI_HOST, &spi_bus_cfg, 0)) {
		ret = -1;
		goto out;
	}
	if (spi_bus_add_device(HSPI_HOST, &spi_if_cfg, &dev->spi_hndl)) {
		ret = -2;
		goto out;
	}

	dev->ll_readl = swd_ll_readl;
	dev->ll_writel = swd_ll_writel;

	swd_ll_init_seq(dev);
out:
	if (ret)
		printf("%s: %d\n", __func__, ret);
	return ret;

}
